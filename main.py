'''
Courtesy of: 
https://stackoverflow.com/questions/4260116/find-size-and-free-space-of-the-filesystem-containing-a-given-file)
'''

from DiskSpace import DiskSpace

def main():
    ds = DiskSpace()
    freeSpaceSize = ds.get_fs_freespace('./')
    print(freeSpaceSize)

if __name__ == "__main__":
    main()
