'''
Courtesy of: 
https://stackoverflow.com/questions/4260116/find-size-and-free-space-of-the-filesystem-containing-a-given-file)
'''

import os
class DiskSpace:

    def get_fs_freespace(self, pathname):
        "Get the free space of the filesystem containing pathname"
        stat = os.statvfs(pathname)
        # use f_bfree for superuser, or f_bavail if filesystem
        # has reserved space for superuser
        return stat.f_bfree * stat.f_bsize



